#include <stdio.h>
#include <stdlib.h>

#include "liste.h"

/* Initialisation d'une liste */
Liste liste_initialiser()
{
  return(NULL);
}

/* Test si une liste est vide */
int liste_vide(Liste l)
{
  return (l == NULL);
}

/* Ajout d'un élément en tête de liste */
Liste liste_teteinserer(Liste l, int e)
{
  Liste p;
  
  p = (Liste) malloc(sizeof(TypeCellule));
  if (p == NULL) {
    printf("Allocation impossible...\n"); 
    exit(1);
  } 
  p->val = e;
  p->suc = l;

  return (p);
}

/* Suppression d'un élément en tête de liste */
Liste liste_tetesupprimer(Liste l)
{
  Liste p = NULL;
  
  if (!liste_vide(l)) {
    p = l->suc;
    free(l);
  }

  return (p) ;
}


/* Affichage d'une liste */

void liste_afficher(Liste l)
{
  printf("Affichage de la liste\n");
  
  if (!l)
    printf("Liste vide...\n");
  else
    while(l) {
      printf("%d\n", l->val);
      l = l->suc;
    }
}

/* Insertion en queue de liste */

Liste liste_queue_inserer(Liste l, int v)
{
  if (!l)
    return liste_teteinserer(l, v);

  else {
    Liste p;
    Liste tete = l;

    /* 1) Localisation dernier élément */

    while(l->suc)
      l = l->suc;
    
    /* 2) Allocation nouvel élément + init */
  
    p = (Liste) malloc(sizeof(TypeCellule));
    if (p == NULL) {
      printf("Allocation impossible...\n"); 
      exit(1);
    } 
    p->val = v;
    p->suc = NULL;
    
    /* 3) MAJ chaînage */

    l->suc = p;
    
    /* 4) Retour nouvelle tête de liste */
    return tete;
  }
}
