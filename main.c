#include <stdio.h>
#include "liste.h"

int main()
{
  Liste l;

  l = liste_initialiser();
  liste_afficher(l);
  l = liste_teteinserer(l, 6);
  liste_afficher(l);
  l = liste_queue_inserer(l, 9);
  liste_afficher(l);
  
  return 0;
}
